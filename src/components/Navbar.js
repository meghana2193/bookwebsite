import React from 'react';

import { Link } from 'react-router-dom';

 const Navbar = ()=>{
    return(
            <nav className="navigation">
               
                <div className= "navigation">
                    <Link to="/" text-align="center">BOOK STORE</Link>
                    <ul className="left">
                    <li><Link to="/">Log in</Link></li>
                    </ul>
                    <ul className="right">
                        <li><Link to="/">Shop Books</Link></li>
                       
                        <li><Link to="/cart">Cart</Link></li>
                      
                    </ul>
                </div>
            </nav>
   
        
    )
}

export default Navbar;