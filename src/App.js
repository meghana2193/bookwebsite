import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './components/Home'
import Navbar from './components/Navbar'

import Cart from './components/Cart'
import Carousel from './components/2carousel/Carousel';
import Main from './components/3main/Main';

class App extends Component {
  render() {
    return (
      <div>
         
        <BrowserRouter>
            <div className="App">
            
              <Navbar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/cart" component={Cart}/>
                  </Switch>
             </div>
       </BrowserRouter>
      
        <Main></Main>
       
      </div>
    );
  }
}

export default App;
